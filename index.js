const FIRST_NAME = "ILEANA";
const LAST_NAME = "ZAVOIANU";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
function initCaching() 
{
    var gol={};
    gol.pageAccessCounter=function(site='home')
    {
        site=new String(site).toLowerCase();
        if(gol.hasOwnProperty(site))
        {
            gol[site]++;
        }
        else
        {
            Object.defineProperty(gol,site,{
                value:1,
                writable:true
            });
        }
    }
    gol.getCache=function()
    {
        return this;
    }
    return gol;   
}

module.exports = 
{
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}